public class MainObat {
    public static void main(String[] args) {
        Obat obat = new Obat();
        obat.setId(1);
        obat.setHarga(3000);
        obat.setNama("Oskadon");

        System.out.println("Id: " + obat.getId());
        System.out.println("Nama: " + obat.getNama());
        System.out.println("Harga: " + obat.getHarga());
    }
}
